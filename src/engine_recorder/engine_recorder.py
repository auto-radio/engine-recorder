"""AuRa Engine Recorder to record and save the playout of the AuRa Engine.

This module uses FFMPEG to capture the output from the AuRa engine. The recordings are saved in
blocks. The recorder is configurable via the config/engine-recorder.yaml file. Among other things,
the audio quality, the length of the audio blocks or the output format can be set here. Optional
functionalities of the recorder are the synchronization of the recordings in another location
and the automatic deletion of files older than a configurable period of time.

    Typical usage example:

    recorder = Recorder()
    recorder.setup()
    recorder.start()
"""

from __future__ import annotations

import importlib.metadata
import logging
import os
import random
import re
import signal
import subprocess
import sys
import time
from datetime import date, timedelta
from typing import List

import schedule
import yaml

import src.engine_recorder.select_pcm as select_pcm


class EnvVarLoader(yaml.SafeLoader):
    """Wrapper class for yaml.SafeLoader."""

    pass


class Recorder:
    """The Recorder which uses FFMPEG to capture the configured output.

    This class mainly consists of a call to Popen. It reads the
    config/engine-recorder.yaml config file to build a list of arguments for FFMPEG. This is then
    fed to Popen. The FFMPEG process then runs in the background saving the output of the given
    input source. While the Popen process is running in the background the Recorder periodically
    runs, if enabled, a sync process and a check for old recordings to be deleted.

    Attributes:
        name: A string which is used to name the virtual audio device.
            The default is 'AuRa_Recorder'
        config_file : A string describing the path to the config file.
            The default config file is 'config/engine-recorder.yaml'
    """

    def __init__(self, name: str = "AuRa_Recorder", config_file: str | None = None):
        """Inits Recorder with AuRa_Recorder or another name if given."""
        self._home_path = os.path.expanduser("~")
        self._recorder_root = os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        )
        self.pcm_device_name = name
        if not config_file:
            config_file = os.path.join(self._recorder_root, "config/engine-recorder.yaml")
        self.config_file = config_file
        self._stop_recording = False
        self.archiver = None

    def setup(self):
        """Prepares the Recorder.

        Reads the config file and if it is valid sets necessary options. Exit if any config
        is invalid.
        """
        if os.path.exists("/etc/aura/engine-recorder.yaml"):
            self.config_file = "/etc/aura/engine-recorder.yaml"
        try:
            config = self.read_yaml_config(self.config_file)
        except ConfigError:
            print(f"Error: Could not read config file {self.config_file}.")
            sys.exit(1)

        self.config = Config(config)

        LogHandler(self.config.logger)
        self.logger = logging.getLogger(self.config.logger.name)

        self.logger.info(f'Loaded yaml file "{self.config_file}".')

        self._startup_message()

        store_dir = self.get_validated_store_dir(self.config.recorder.file.store_dir)
        # FIXME: What do we want to to if the store_dir does not exist
        # but we could create it.

        # if store_dir is None:
        #     os.makedirs(self.config.recorder.file.store_dir, exist_ok=True)
        #     self.logger.warning(
        #         f"Create Audio storage directory '{self.config.recorder.file.store_dir}'."
        #     )
        store_dir = self.get_validated_store_dir(self.config.recorder.file.store_dir)
        if store_dir:
            self.logger.info(f"Audio storage directory is set to '{store_dir}'.")
            self.config.recorder.file.store_dir = store_dir
        else:
            self.logger.error(
                (
                    f"Audio storage directory '{self.config.recorder.file.store_dir}'"
                    " is not accessible or does not exist!"
                )
            )
            sys.exit(1)

        audio_input = self.config.recorder.input.source
        audio_input = self.get_validated_audio_input(audio_input)
        self.config.recorder.input.source = audio_input
        self.logger.info(f"Audio input device is set to '{audio_input}'")

        if self.validate_virtual_audio_device(self.config.recorder):
            self.logger.info("Virtual audio device enabled.")
            if self.create_virtual_audio_device(self.config.recorder):
                self.logger.info("Created Virtual audio device.")
            else:
                self.logger.error("Could not create virtual audio device.")
                sys.exit(1)

        self.recorder_cmd = self.build_record_command(self.config.recorder)

        # we could allow the user to change this
        # store_dir_sub_dir_strftime = "%Y/%m/%d"
        store_dir_sub_dir_strftime = self.config.recorder.file.store_dir_sub_dir

        try:
            self.create_dirs(store_dir, store_dir_sub_dir_strftime)
        except PermissionError:
            self.logger.error(
                f"Permission denied while trying to create subdirectories in {store_dir}"
            )
            sys.exit(1)
        schedule.every().day.do(lambda: self.create_dirs(store_dir, store_dir_sub_dir_strftime))

        if self.config.sync.sync:
            self.logger.info("Archiver enabled. ")
            self.archiver = Archiver(self.config.logger)
            self.archiver.options = self.config.sync.options
            # this could be configurable for day, hour, minute, etc
            schedule.every().hour.do(
                lambda: self.archiver.sync(
                    self.config.recorder.file.store_dir, self.config.sync.destination
                )
            )
        else:
            self.logger.info("Archiver disabled.")

        if self.config.delete.delete:
            self.logger.info(
                (
                    "Deleting old files enabled. "
                    f"Delete all files in {self.config.recorder.file.store_dir} older "
                    f"then {self.config.delete.older_than} days."
                )
            )
            # this could be configurable for day, hour, minute, etc
            schedule.every().hour.do(
                lambda: self.delete_old_files(
                    self.config.recorder.file.store_dir, self.config.delete.older_than
                )
            )
        else:
            self.logger.info("Deleting old files disabled.")

    def read_yaml_config(self, file_path: str) -> dict:
        """Reads a yaml config file and return it as a dictionary.

        Args:
            file_path: A string describing the path to the yaml file.

        Returns:
            A dict containing the config options of the yaml file.

        Raises:
            ConfigError: An error occurred reading the config file.
        """
        # TODO: move this into a class called App?
        # then we could init Recorder with the right Config object

        path_matcher = re.compile(r"\$\{([^}^{]+)\}")

        def path_constructor(loader, node):
            return os.path.expandvars(node.value)

        EnvVarLoader.add_implicit_resolver("!path", path_matcher, None)
        EnvVarLoader.add_constructor("!path", path_constructor)

        try:
            with open(file_path, "r") as stream:
                return yaml.load(stream, Loader=EnvVarLoader)
        except yaml.YAMLError:
            print(f"Error: A problem occurred trying to read the YAML config file {file_path}.")
            raise ConfigError(f"Could not read the YAML config file {file_path}")
        except FileNotFoundError:
            print(f"Error: Could not find {file_path}.")
            raise ConfigError(f"Could not find {file_path}.")
        except PermissionError:
            print(f"Error: Permission denied while trying to access {file_path}.")
            raise ConfigError(f"Permission denied while trying to access {file_path}")

    def create_dirs(self, store_dir: str, sub_dir_strftime: str) -> bool:
        """Creates the recording directories for the next days.

        Creates new directories in store_dir according to sub_dir_strftime. These directories
        are later used to store the audio blocks.

        Args:
            store_dir: A string describing the file path where the directories will be created.
            sub_dir_strftime: A strftime string describing the hierarchy of the sub directories.
                For example '%Y/%m/%d' would create a tree '2022/01/01'.

        Returns:
            True, if creating the directories was successful.

        Raises:
            PermissionError: If creating directories is not allowed in store_dir.
        """
        today = date.today()
        for day_offset in range(2):
            day = date(today.year, today.month, today.day) + timedelta(day_offset)
            dir = f"{store_dir}/{day.strftime(sub_dir_strftime)}"
            try:
                os.makedirs(dir)
            except FileExistsError:
                pass
            except PermissionError:
                self.logger.error(f"Could not create {dir}.")
                raise
            else:
                self.logger.info(f"Created {dir}")

        return True

    def start(self):
        """Starts the recording process.

        This makes a call to subprocess.Popen and starts a process according to
        self.recorder_cmd.
        """
        signal.signal(signal.SIGINT, self._exit_graceful)
        signal.signal(signal.SIGTERM, self._exit_graceful)
        command_list = self.recorder_cmd
        cmd_string = '" "'.join(command_list[1:])
        self.logger.info(f'Try to start recorder with: {command_list[0]} "{cmd_string}"')
        return_code = 0
        try:
            with subprocess.Popen(
                command_list,
                text=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            ) as recorder_process:
                time.sleep(3)
                # check if the process is still running, if not raise exception
                return_code_recorder = recorder_process.poll()
                if return_code_recorder is not None:
                    stdout, stderr = recorder_process.communicate()
                    raise subprocess.CalledProcessError(
                        return_code_recorder,
                        recorder_process.args,
                        stderr=stderr,
                    )

                self.logger.info("Recording started")
                while not self._stop_recording:
                    schedule.run_pending()
                    time.sleep(5)

                # this only happens when we stop the recorder

                # give ffmpeg up to 10 seconds to exit
                try:
                    # try to tell ffmpeg to exit graceful
                    stdout, stderr = recorder_process.communicate("q", 10)
                    self.logger.info(f"FFmpeg finished: {stderr}")

                # otherwise we kill it
                except subprocess.TimeoutExpired:
                    self.logger.warning("Could not exit ffmpeg gracefully, killing it now.")
                    recorder_process.kill()
                    stdout, stderr = recorder_process.communicate()
                    # stdout is always empty because ffmpeg does not write to it
                    self.logger.warning(f"Stderr: {stderr}")
                    return_code = 1
        except subprocess.CalledProcessError as cpe:
            self.logger.error(
                f"Could not start recorder. \n{cpe.stderr}\n{' '.join(command_list)}"
            )
            self.logger.error(f"Exiting: {cpe.returncode}")
            return_code = cpe.returncode
        except FileNotFoundError as fnfe:
            self.logger.error(
                f"Process failed to start because ffmpeg could not be found."
                f"\n{' '.join(command_list)}\n{fnfe}"
            )
            return_code = 1
        finally:
            self._shutdown_message()

        sys.exit(return_code)

    def _exit_graceful(self, signum, frame):
        """Stops the recording and runs all schedules one last time when a Signal is caught."""
        self.logger.info(
            f"Received signal {signum}, the Recorder is now trying to exit gracefully."
        )
        self._stop_recording = True
        schedule.run_all()

    def version(self) -> str:
        """Reads and returns the contents the version mentioned in pyproject.toml.

        Returns:
            A string describing the current version of the Engine Recorder.
        """
        return importlib.metadata.version("engine-recorder")

    def _startup_message(self):
        self.logger.info(f"Engine Recorder v:{self.version()}")

    def _shutdown_message(self):
        self.logger.info("Exiting Engine Recorder")

    def validate_dir(self, dir: str) -> bool:
        """Checks if the given directory exists and can be accessed.

        This uses os.path.isdir() and os.access() underneath.

        Args:
            dir: A String describing the file path to the directory to check.

        Returns:
            True, if the dir exists and is accessible.
            False, if not.
        """
        return os.path.isdir(dir) and os.access(dir, os.W_OK)

    def get_validated_store_dir(self, store_dir: str) -> str | None:
        """Check if the store_dir is set, if so validate it.

        If store_dir is None use the standard directory.

        Args:
            store_dir: A String describing the directory to validate.

        Returns:
            The file path of the directory where to store the audio files.
            None, if the file path could not be validated. See validate_dir().
        """
        if not store_dir:
            store_dir = os.path.join(self._recorder_root, "audio/recordings/block")
        store_dir = store_dir.rstrip("/")
        if self.validate_dir(store_dir):
            return store_dir
        else:
            return None

    def _create_dsnoop_config(self, pcm_device: str, channels: int) -> str:
        """Returns a string describing the configuration of a dsnoop slave.

        Args:
            pcm_device: A String describing the PCM device which it "clones".
            channels: A int as the number of channels for the PCM device.

        Returns:
            A String describing the configuration for the dsnoop slave.
        """
        ipc_key = random.randint(1111111, 9999999)
        config = f"""pcm.{self.pcm_device_name} {{
    type dsnoop
    ipc_key {ipc_key}
    slave {{
        pcm "{pcm_device}"
        {'channels ' if channels else ''}{channels if channels else ''}
    }}
    hint.description "Dsnoop AuRa Recorder used to record the playout"
}}"""
        return config

    def _write_dsnoop_config(self, dsnoop_config: str):
        """Writes a dsnoop config into the ~/.asoundrc file.

        Checks if there is a existing AuRa config in the ~/.asoundrc, if there is one it tries to
        overwrite it. If not it creates a new one.

        Args:
            dsnoop_config (str): The dsnoop PCM device config which will be written to ~/.asoundrc.
        """
        asoundrc_config = ""
        try:
            asoundrc_config = self._read_asoundrc_config()

            # check if there already exists an AuRa_Recorder
            if self.pcm_device_name in asoundrc_config:
                self.logger.info(f"Remove old asoundrc config in {self._home_path}/.asoundrc")
                index_aura_recorder = asoundrc_config.find(self.pcm_device_name) - 4

                # keep everything from the old config except the AuRa one
                first_half = asoundrc_config[:index_aura_recorder]
                # the leftover string now has the old AuRa config at the beginning
                recorder_at_first_place = asoundrc_config[index_aura_recorder:]

                # find the index of the second "}" so that we know where the old config ends
                index_curly_bracket = (
                    recorder_at_first_place.find("}", recorder_at_first_place.find("}") + 1) + 2
                )
                # keep everything after that
                second_half = recorder_at_first_place[index_curly_bracket:]

                # plug everything back together but without the old AuRa config
                asoundrc_config = f"{first_half}{second_half.lstrip()}"
            asoundrc_config = f"{dsnoop_config}\n{asoundrc_config}"

        # there is no ~/.asoundrc file yet -> create one
        except FileNotFoundError:
            self.logger.info(f"Create new asoundrc config {self._home_path}/.asoundrc")
            asoundrc_config = dsnoop_config
        finally:
            self._write_asoundrc_config(asoundrc_config)

    def _write_asoundrc_config(self, config: str):
        with open(f"{self._home_path}/.asoundrc", "w") as file:
            file.write(config)
            self.logger.info(f"Write asoundrc config in {self._home_path}/.asoundrc")

    def _read_asoundrc_config(self) -> str:
        with open(f"{self._home_path}/.asoundrc", "r") as file:
            asoundrc_file = file.read()
            self.logger.info(f"Read asoundrc config in {self._home_path}/.asoundrc")
        return asoundrc_file

    def delete_old_files(self, store_dir: str, older_than: int) -> bool:
        """Deletes all recordings older then a predefined time period.

        Checks the mtime of files in store_dir and if it is older then the predefined time period,
        delete them.

        Args:
            store_dir: The file path as a string to the directory to check for files to delete.
            older_than: The minimum age of the files in days which will be deleted.

        Returns:
            True, if the method could delete all files older than the minimum age.
            This is also True if no files were found and therefore no files were deleted.

            False, if the method encounters an FileNotFoundError or PermissionError
            for at least one file.
        """
        now = time.time()
        self.logger.info(
            f"Delete: Looking for files older then {older_than}" f" days in: {store_dir}."
        )
        no_deletion_problems = True
        for root, dirs, files in os.walk(store_dir):
            for file in files:
                try:
                    if file == ".gitignore":
                        continue
                    file = os.path.join(root, file)
                    # check if the mtime is older then x days (x * hours * minutes * seconds)
                    if os.stat(file).st_mtime < now - (older_than * 24 * 60 * 60):
                        os.remove(file)
                        self.logger.info(f"Delete:\t{file}")
                except FileNotFoundError:
                    self.logger.warning(
                        f'Could not find "{file}". Continue without deleting this file.'
                    )
                    no_deletion_problems = False
                except PermissionError:
                    self.logger.warning(
                        f'Permission denied when trying to delete "{file}".'
                        " Continue without deleting this file."
                    )
                    no_deletion_problems = False
        return no_deletion_problems

    def is_web_stream(self, input_source: str) -> bool:
        """Check if a string is a web stream.

        Args:
            input_source: A String describing the input to check.

        Returns:
            True, if input_source contains 'http'.
            False, if input_source is no web stream.
        """
        return "http" in input_source

    def validate_virtual_audio_device(self, config: Config.Audio) -> bool:
        """Validate the virtual audio device.

        Args:
            config: A Config object describing the audio configuration.

        Returns:
            True, if a virtual audio device is wanted.
            False, if no virtual audio device is wanted.
        """
        if config.input.virtual_device:
            self.logger.info("Virtual audio device enabled.")
            input_source = config.input.source

            if self.is_web_stream(input_source):
                self.logger.warning(
                    f"Input source {input_source} is a web stream. Skip virtual audio device."
                )
                return False
            return True
        return False

    def create_virtual_audio_device(self, config: Config.Audio) -> bool:
        """Create a virtual audio device.

        Args:
            config: The settings by which the virtual audio device is to be created.

        Returns:
            True, if creating the device was successful.
            False, if creating the device failed.
        """
        dsnoop_config = self._create_dsnoop_config(config.input.source, config.input.channels)
        try:
            self._write_dsnoop_config(dsnoop_config)
        except PermissionError:
            self.logger.error(
                f"Permission denied when trying to open '{self._home_path}/.asoundrc'."
            )
            return False
        else:
            return True

    def get_validated_audio_input(self, input_source: str) -> str:
        """Return the input source if set or select one with the cli.

        Checks if there is an input source given, if not select one with the cli and return it.

        Args:
            input_source: A string describing the audio input source.

        Returns:
            A string describing the audio input source.
        """
        if not input_source:
            input_source = select_pcm.select_pcm()

        return input_source

    def build_record_command(self, config: Config.Audio) -> List[str]:
        """Build the command used to start the recorder as a list from the given Config Audio.

        This builds a list of strings by adding strings from the Config.Audio object if the values
        are set.

        Args:
            config: A Config.Audio instance.

        Returns:
            A list of strings which is later passed as an argument to subprocess.Popen.
        """
        # check if we have a web stream as input, if so ignore source_format and sample_format
        if self.is_web_stream(config.input.source):
            config.input.source_format = None
            config.input.sample_format = None
            config.input.sample_rate = None
            config.input.codec = None
        record_cmd_ffmpeg = [
            "nice",
            "-n",
            "18",
            "ionice",
            "-c",
            "2",
            "-n",
            "0",
            "/usr/bin/ffmpeg",
            "-hide_banner",
            "-loglevel",
            "error",
            "-f" if config.input.source_format else "",
            config.input.source_format,
            "-acodec" if config.input.sample_format else "",
            config.input.sample_format,
            "-ac" if config.input.channels else "",
            config.input.channels,
            "-ar" if config.input.sample_rate else "",
            config.input.sample_rate,
            "-codec" if config.input.codec else "",
            config.input.codec,
            "-i" if config.input.source else "",
            config.input.source,
            "-codec" if config.output.codec else "",
            config.output.codec,
            "-ab" if config.output.bitrate else "",
            config.output.bitrate,
            "-f",
            "segment",
            "-segment_atclocktime",
            "1",
            "-segment_time",
            config.file.segment_time,
            "-strftime",
            "1",
            (
                f"{config.file.store_dir}/{config.file.store_dir_sub_dir}"
                f"/{config.file.strftime}.{config.file.format}"
            ),
        ]

        # remove all empty strings and return the list
        return [f"{x}" for x in record_cmd_ffmpeg if x]


class Config:
    """Configuration class to map a dictionary values to certain variables.

    Attributes:
        raw: A dictionary to map.
    """

    def __init__(self, raw: dict):
        """Set config from dict."""
        self.recorder = self.Audio(raw["Audio"])
        self.sync = self.Sync(raw["Sync"])
        self.delete = self.Delete(raw["Delete"])
        self.logger = self.Logger(raw["Logger"])

    class Audio:
        """Audio Configuration."""

        def __init__(self, raw: dict):
            """Set config from dict."""
            self.input = self.Input(raw["input"])
            self.output = self.Output(raw["output"])
            self.file = self.File(raw["file"])

        class Input:
            """Input Configuration."""

            def __init__(self, raw: dict):
                """Set config from dict."""
                self.source_format = raw["source_format"]
                self.sample_format = raw["sample_format"]
                self.channels = raw["channels"]
                self.sample_rate = raw["sample_rate"]
                self.codec = raw["codec"]
                self.source = raw["source"]
                self.virtual_device = raw["virtual_device"]

        class Output:
            """Output Configuration."""

            def __init__(self, raw: dict):
                """Set config from dict."""
                self.codec = raw["codec"]
                self.bitrate = raw["bitrate"]

        class File:
            """File Configuration."""

            def __init__(self, raw: dict):
                """Set config from dict."""
                try:
                    self.segment_time = int(raw["segment_time"])
                except (ValueError, TypeError):
                    self.segment_time = None
                self.strftime = raw["strftime"]
                self.format = raw["format"]
                self.store_dir = raw["store_dir"]
                self.store_dir_sub_dir = raw.get("store_dir_sub_dir", "%Y/%m/%d")

    class Sync:
        """Sync Configuration."""

        def __init__(self, raw: dict):
            """Set config from dict."""
            try:
                self.sync = str(raw["sync"]) == "True"
            except ValueError:
                self.sync = False
            self.destination = raw["destination"]
            self.options = raw["options"]

    class Delete:
        """Delete Configuration."""

        def __init__(self, raw: dict):
            """Set config from dict."""
            try:
                self.delete = str(raw["delete"]) == "True"
            except ValueError:
                self.delete = False
            try:
                self.older_than = int(raw["older_than"])
            except (ValueError, TypeError):
                self.older_than = None

    class Logger:
        """Logger Configuration."""

        def __init__(self, raw: dict):
            """Set config from dict."""
            self.name = raw["name"]
            self.file = raw["file"]
            self.level = raw["level"]


class ConfigError(Exception):
    """Config exception."""


class LogHandler:
    """Log Handler for writing logs."""

    config = None
    logger = None

    def __init__(self, config: Config.Logger):
        """Init the Log Handler."""
        self.config = config
        name = self.config.name

        if self.config.file:
            log_file = self.config.file
        else:
            project_root = os.path.dirname(
                os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            )
            log_file = os.path.join(project_root, "log/engine-recorder.log")
        level = self.get_level(self.config.level)

        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)

        if not self.logger.hasHandlers():
            file_handler = logging.FileHandler(log_file)
            date_part = "%(asctime)s:%(name)s:%(levelname)s"
            message = " - %(message)s"
            if level == logging.DEBUG:
                filepart = " - [%(filename)s:%(lineno)s-%(funcName)s()]"
            else:
                filepart = ""
            formatter = logging.Formatter(f"{date_part}{message}{filepart}")
            file_handler.setFormatter(formatter)
            file_handler.setLevel(level)

            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(level)
            stream_handler.setFormatter(formatter)

            self.logger.setLevel(level)
            self.logger.addHandler(file_handler)
            self.logger.addHandler(stream_handler)

            self.logger.debug("Added file and stream handlers to logger")
        else:
            self.logger.debug("Reuse file and stream handlers")

    def get_level(self, level: str) -> int:
        """Returns the log level as an int."""
        if level == "DEBUG":
            return logging.DEBUG
        elif level == "INFO":
            return logging.INFO
        elif level == "WARNING":
            return logging.WARNING
        elif level == "ERROR":
            return logging.ERROR
        elif level == "CRITICAL":
            return logging.CRITICAL
        else:
            return logging.INFO


class Archiver:
    """Archiver used to archive audio blocks to a location.

    Attributes:
        log_config: A Config.Logger for logging.
    """

    def __init__(self, log_config: Config.Logger):
        """Init Archiver with the command used for archiving."""
        self.command = "/usr/bin/rsync"
        self.options = ""
        LogHandler(log_config)
        self.logger = logging.getLogger(log_config.name)
        self.logger.info(f"Archiver: Archiver initialized with {self.command}")

    def sync(self, src_folder: str, target_folder: str) -> bool:
        """Synchronies files from src_folder to target_folder.

        Args:
            src_folder: A String describing the source folder to synchronies.
            target_folder: A String describing the target folder to synchronies to.

        Returns:
            True, if the synchronization was successful.
            False, if not.

        Raises:
            FileNotFoundError: An error if rsync was not found.
        """
        sync_cmd = [
            self.command,
            self.options,
            "--info=name1",
            src_folder,
            target_folder,
        ]

        try:
            self.logger.info(f"Archiver: \"{' '.join(sync_cmd)}\"")
            rsync_process = subprocess.run(sync_cmd, check=True, capture_output=True, text=True)
            for line in rsync_process.stdout.strip().splitlines():
                self.logger.info(f"Archiver: \t{line}")
            return True
        except subprocess.CalledProcessError as cpe:
            self.logger.warning(f"Archiver: Could not run rsync:\n{cpe.stderr}")
        except FileNotFoundError:
            self.logger.error(f"Archiver: Could not find {self.command}.")
            raise
        return False


def main():
    """Main function to start a recorder."""
    recorder = Recorder()
    recorder.setup()
    recorder.start()


if __name__ == "__main__":
    main()
