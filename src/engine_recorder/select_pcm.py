"""Select a PCM device via CLI."""

import subprocess


def select_pcm():
    """Returns a pcm device selected via CLI."""
    process = subprocess.Popen(["arecord", "-L"], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()

    # split the PCM devices and drop the last one because it is empty
    arecord_result = stdout.decode("utf-8").split("\n")[:-1]
    pcm_list = [x for x in arecord_result if "    " not in x]

    for index, pcm in enumerate(pcm_list):
        print(f"[{index}]: '{pcm}'")

    # wait for user input
    while True:
        try:
            user_pcm_index = int(input("Enter the index of the PCM you want to use: "))
            print(f"You selected [{user_pcm_index}]: {pcm_list[user_pcm_index]}")
            break
        except ValueError:
            print("Please enter a Integer!")
        except IndexError:
            print("The index is out of range. Please enter a valid index!")
    return pcm_list[user_pcm_index]
