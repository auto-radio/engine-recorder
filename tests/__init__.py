"""Provides access to the src folder."""

import os
import sys

project_path = os.getcwd()
source_path = os.path.join(project_path, "src")
sys.path.append(source_path)
