"""Archiver test file."""
import logging
import os
import unittest

import src.engine_recorder.engine_recorder as recorder


class TestArchiver(unittest.TestCase):
    """Archiver test class."""

    config_dict = {"name": None, "file": None, "level": None}
    logger_config = recorder.Config.Logger(config_dict)
    logging.disable(logging.CRITICAL)

    def test_sync(self):
        """Simple sync test."""
        self.assertFalse(os.path.isdir("tmptest"))
        self.assertFalse(os.path.isdir("tmptest/src"))
        self.assertFalse(os.path.isdir("tmptest/target"))
        os.mkdir("tmptest")
        os.mkdir("tmptest/src")
        os.mkdir("tmptest/target")
        self.assertTrue(os.path.isdir("tmptest/src"))
        self.assertFalse(os.path.exists("tmptest/src/test.audio"))
        with open("tmptest/src/test.audio", mode="a"):
            pass

        self.assertTrue(os.path.exists("tmptest/src/test.audio"))

        archiver = recorder.Archiver(self.logger_config)

        archiver.options = "-a"
        self.assertTrue(archiver.sync("tmptest/src/", "tmptest/target"))

        self.assertTrue(os.path.exists("tmptest/target/test.audio"))

        os.remove("tmptest/target/test.audio")
        os.remove("tmptest/src/test.audio")
        os.rmdir("tmptest/src")
        os.rmdir("tmptest/target")
        os.rmdir("tmptest")

        self.assertFalse(os.path.isdir("tmptest"))

    def test_sync_fail(self):
        """Fail sync on purpose."""
        archiver = recorder.Archiver(self.logger_config)
        self.assertFalse(archiver.sync("", ""))
        archiver.command = "/not_rsync"
        self.assertRaises(FileNotFoundError, archiver.sync, "config", "/tmp")


if __name__ == "__main__":
    unittest.main()
