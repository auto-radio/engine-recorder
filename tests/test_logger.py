"""Log Handler test."""
import logging
import unittest

import src.engine_recorder.engine_recorder as recorder


class TestLogHandler(unittest.TestCase):
    """Log Handler test class."""

    def test_log_handler(self):
        """Simple log test."""
        raw_dict = {"name": "log_test", "file": None, "level": "INFO"}
        log_config = recorder.Config.Logger(raw_dict)

        recorder.LogHandler(log_config)
        logger = logging.getLogger(log_config.name)

        self.assertIsInstance(logger, logging.Logger)

    def test_log_levels(self):
        """Test all log levels."""
        raw_dict = {"name": "log_test", "file": None, "level": "INFO"}
        log_config = recorder.Config.Logger(raw_dict)
        log_handler = recorder.LogHandler(log_config)

        self.assertEqual(log_handler.get_level("DEBUG"), 10)
        self.assertEqual(log_handler.get_level("INFO"), 20)
        self.assertEqual(log_handler.get_level("WARNING"), 30)
        self.assertEqual(log_handler.get_level("ERROR"), 40)
        self.assertEqual(log_handler.get_level("CRITICAL"), 50)
        self.assertEqual(log_handler.get_level("ANYTHING"), 20)

    def test_log_debug_level(self):
        """Test the filepart of the debug log level."""
        raw_dict = {"name": "log_test_debug", "file": None, "level": "DEBUG"}
        log_config = recorder.Config.Logger(raw_dict)
        recorder.LogHandler(log_config)
        logger = logging.getLogger(log_config.name)

        self.assertIsInstance(logger, logging.Logger)

    def test_log_file_location(self):
        """Test the filelocation of the log file."""
        raw_dict = {"name": "log_test", "file": "test.log", "level": "DEBUG"}
        log_config = recorder.Config.Logger(raw_dict)
        recorder.LogHandler(log_config)
        logger = logging.getLogger(log_config.name)

        self.assertIsInstance(logger, logging.Logger)


if __name__ == "__main__":
    unittest.main()
