"""Dummy test file."""
import unittest

import src.engine_recorder.engine_recorder as recorder


class TestConfig(unittest.TestCase):
    """Simple dummy test class."""

    config_dict = {
        "Audio": {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": None,
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": None,
                "strftime": None,
                "format": None,
                "store_dir": None,
            },
        },
        "Sync": {
            "sync": None,
            "destination": None,
            "options": None,
        },
        "Delete": {"delete": None, "older_than": None},
        "Logger": {"name": None, "file": None, "level": None},
    }

    def test_config(self):
        """Create Config object test."""
        try:
            recorder.Config(self.config_dict)
        except KeyError:
            self.fail("Missing key! Check yaml file and Config class")


if __name__ == "__main__":
    unittest.main()
