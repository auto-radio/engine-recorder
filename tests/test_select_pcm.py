"""select_pcm test file."""
import unittest
from io import StringIO
from unittest import mock

from src.engine_recorder.select_pcm import select_pcm


class TestSelectPcm(unittest.TestCase):
    """Select pcm via CLI test class."""

    @mock.patch("sys.stdout", new_callable=StringIO)
    def test_select_pcm_name(self, mock_stdout):
        """Testing selecting pcm via cli and arecord."""
        with mock.patch("builtins.input", side_effect=["0"]):
            pcm_device_name = select_pcm()
            self.assertEqual(pcm_device_name, "null")

    @mock.patch("sys.stdout", new_callable=StringIO)
    def test_select_pcm_stdout(self, mock_stdout):
        """Test stdout for selecting a pcm device."""
        with mock.patch("builtins.input", side_effect=["0"]):
            select_pcm()
            output = str(mock_stdout.getvalue())
            last_line = output.splitlines()[-1]
            self.assertEqual(last_line, "You selected [0]: null")

    @mock.patch("sys.stdout", new_callable=StringIO)
    def test_select_pcm_wrong_index(self, mock_stdout):
        """Test wrong index for selecting a pcm device."""
        with mock.patch("builtins.input", side_effect=["a", 999, 0]):
            select_pcm()
            output = str(mock_stdout.getvalue())
            wrong_value = output.splitlines()[-3]
            self.assertEqual(wrong_value, "Please enter a Integer!")
            wrong_index = output.splitlines()[-2]
            self.assertEqual(wrong_index, "The index is out of range. Please enter a valid index!")


if __name__ == "__main__":
    unittest.main()
