"""YAML test file."""
import os
import unittest
from unittest import mock

import src.engine_recorder.engine_recorder as recorder


class TestYaml(unittest.TestCase):
    """Simple YAML test class."""

    tests_root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    yaml_path = os.path.join(tests_root_dir, "config/sample-engine-recorder.yaml")
    recorder = recorder.Recorder()

    def test_read_sample_yaml_config(self):
        """Reads the sample yaml file in engine-recorder/config/sample-engine-recorder.yaml."""
        yaml_dict = self.recorder.read_yaml_config(self.yaml_path)
        self.assertIsInstance(yaml_dict, dict)
        try:
            recorder.Config(yaml_dict)
        except KeyError as ke:
            self.fail(f"Could not create Config from dict: {yaml_dict}.\n{ke}")

    def test_read_docker_yaml_config(self):
        """Reads the docker yaml file."""
        docker_path = os.path.join(self.tests_root_dir, "config/for-docker.yaml")
        yaml_dict = self.recorder.read_yaml_config(docker_path)
        self.assertIsInstance(yaml_dict, dict)
        try:
            recorder.Config(yaml_dict)
        except KeyError as ke:
            self.fail(f"Could not create Config from dict: {yaml_dict}.\n{ke}")

    def test_read_faulty_yaml_config(self):
        """Reads faulty yaml files in engine-recorder/tests/config/."""
        faulty_dir = os.path.join(self.tests_root_dir, "tests/config")
        faulty_file = os.path.join(faulty_dir, "faulty-engine-recorder.yaml")

        self.assertRaises(recorder.ConfigError, self.recorder.read_yaml_config, faulty_file)
        self.assertRaises(recorder.ConfigError, self.recorder.read_yaml_config, "not_found.yaml")

    def test_read_yaml_config_permission_denied(self):
        """Mocks open() to raise a PermissionError."""
        with mock.patch("builtins.open") as mock_oserror:
            mock_oserror.side_effect = PermissionError
            self.assertRaises(
                recorder.ConfigError, self.recorder.read_yaml_config, "/test/permission_denied"
            )


if __name__ == "__main__":
    unittest.main()
