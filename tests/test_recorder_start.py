"""Test recorder.start with mocking subprocess."""
import subprocess
import unittest
from unittest import mock
from unittest.mock import MagicMock, patch

import schedule
from mockproc import mockprocess

import src.engine_recorder.engine_recorder as recorder


class Test(unittest.TestCase):
    """Test recorder.start with mocking subprocess.Popen."""

    def setUp(self):
        """Setup."""
        self.scripts = mockprocess.MockProc()

    config_dict = {
        "Audio": {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": None,
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": None,
                "strftime": None,
                "format": None,
                "store_dir": None,
            },
        },
        "Sync": {
            "sync": None,
            "destination": None,
            "options": None,
        },
        "Delete": {"delete": None, "older_than": None},
        "Logger": {"name": "test_aura", "file": None, "level": None},
    }

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_subprocess_fail(self):
        """Test for subprocess.Popen to fail to start."""
        self.scripts.append("ffmpeg", returncode=1)

        with self.scripts:
            rec = recorder.Recorder()
            rec.setup()
            rec.recorder_cmd = ["ffmpeg"]
            with self.assertRaises(SystemExit) as sys_exit:
                rec.start()
            self.assertEqual(sys_exit.exception.code, 1)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_subprocess_exit(self):
        """Test for subprocess.Popen to exit as intended."""
        self.scripts.append("ffmpeg", returncode=0)

        with self.scripts:
            rec = recorder.Recorder()
            rec.setup()
            rec.recorder_cmd = ["ffmpeg"]
            rec._stop_recording = True
            with self.assertRaises(SystemExit) as sys_exit:
                rec.start()
            self.assertEqual(sys_exit.exception.code, 0)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    @mock.patch(
        "subprocess.Popen.communicate",
        side_effect=[subprocess.TimeoutExpired("cmd", "1.0"), ("in", "err")],
    )
    def test_subprocess_kill_timeout(self, mock_w):
        """Test for subprocess.Popen to not exit properly and trying to kill it."""
        # BUG: the side effect of Popen communicate allows for a potential memory leak
        ffmpeg = """#! /usr/bin/env python
import time
time.sleep( 12.0 )"""
        self.scripts.append("ffmpeg", returncode=0, script=ffmpeg)

        with self.scripts:
            rec = recorder.Recorder()
            rec.setup()
            rec.recorder_cmd = ["ffmpeg"]
            rec._stop_recording = True
            with self.assertRaises(SystemExit) as sys_exit:
                rec.start()
            self.assertTrue(mock_w.called)
            self.assertEqual(sys_exit.exception.code, 1)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    @mock.patch(
        "subprocess.Popen.communicate",
        side_effect=[("in1", "err1"), ("in2", "err2")],
    )
    @mock.patch("subprocess.Popen.kill", return_value=None)
    def test_subprocess_kill_no_timeout(self, mock_w, mock_k):
        """Test for ffmpeg to exit as intended."""
        ffmpeg = """#! /usr/bin/env python
import time
time.sleep( 12.0 )"""
        self.scripts.append("ffmpeg", returncode=0, script=ffmpeg)

        with self.scripts:
            rec = recorder.Recorder()
            rec.setup()
            rec.recorder_cmd = ["ffmpeg"]
            rec._stop_recording = True
            with self.assertRaises(SystemExit) as sys_exit:
                rec.start()

            self.assertEqual(sys_exit.exception.code, 0)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    @mock.patch("subprocess.Popen.communicate", return_value=("stdout", "stderr"))
    @mock.patch("subprocess.Popen.wait", return_value=0)
    @mock.patch("subprocess.Popen.kill", return_value=None)
    @mock.patch("time.sleep", side_effect=[None, InterruptedError])
    def test_subprocess_infinity_loop(self, mock_c, mock_w, mock_k, mock_r):
        """Test for the infinity loop after subprocess.Popen is started."""
        ffmpeg = """#! /usr/bin/env python
import time
time.sleep( 12.0 )"""
        self.scripts.append("ffmpeg", returncode=0, script=ffmpeg)

        with self.scripts:
            rec = recorder.Recorder()
            rec.setup()
            rec.recorder_cmd = ["ffmpeg"]
            with self.assertRaises(InterruptedError):
                rec.start()

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_subprocess_fnfe(self):
        """Test for failing to start subprocess.Popen because the command is unknown."""
        rec = recorder.Recorder()
        rec.setup()
        rec.recorder_cmd = ["nothing"]
        with self.assertRaises(SystemExit) as sys_exit:
            rec.start()

        self.assertEqual(sys_exit.exception.code, 1)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_exit_graceful(self):
        """Test exit graceful."""
        schedule.clear()
        rec = recorder.Recorder()
        self.assertFalse(rec._stop_recording)
        rec.setup()
        rec._exit_graceful(None, None)
        self.assertTrue(rec._stop_recording)
