"""Test recorder."""
import importlib.metadata
import logging
import unittest
from unittest import mock
from unittest.mock import MagicMock, patch

import src.engine_recorder.engine_recorder as recorder


class TestRecorder(unittest.TestCase):
    """Simple dummy test class."""

    logging.disable(logging.CRITICAL)
    rec = recorder.Recorder()
    raw_dict = {"name": "log_test", "file": None, "level": "INFO"}
    log_config = recorder.Config.Logger(raw_dict)
    recorder.LogHandler(log_config)
    logger = logging.getLogger(log_config.name)
    rec.logger = logger

    def test_recorder_version(self):
        """Version test."""
        self.assertEqual(self.rec.version(), importlib.metadata.version("engine-recorder"))

    def test_build_full_record_command(self):
        """Test to build the full cmd for the recording."""
        raw_dict = {
            "input": {
                "source_format": "source_format",
                "sample_format": "sample_format",
                "channels": "channels",
                "sample_rate": "sample_rate",
                "codec": "codec",
                "source": "default",
                "virtual_device": None,
            },
            "output": {"codec": "codec", "bitrate": "bitrate"},
            "file": {
                "segment_time": 1800,
                "strftime": "strftime",
                "format": "flac",
                "store_dir": "/tmp",
            },
        }
        audio_config = recorder.Config.Audio(raw_dict)

        cmd = self.rec.build_record_command(audio_config)
        expected = [
            "nice",
            "-n",
            "18",
            "ionice",
            "-c",
            "2",
            "-n",
            "0",
            "/usr/bin/ffmpeg",
            "-hide_banner",
            "-loglevel",
            "error",
            "-f",
            "source_format",
            "-acodec",
            "sample_format",
            "-ac",
            "channels",
            "-ar",
            "sample_rate",
            "-codec",
            "codec",
            "-i",
            "default",
            "-codec",
            "codec",
            "-ab",
            "bitrate",
            "-f",
            "segment",
            "-segment_atclocktime",
            "1",
            "-segment_time",
            "1800",
            "-strftime",
            "1",
            "/tmp/%Y/%m/%d/strftime.flac",
        ]
        self.assertEqual(cmd, expected)

    def test_build_minimal_record_command(self):
        """Test to build the minimal cmd for the recording."""
        raw_dict = {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": "default",
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": 1800,
                "strftime": "strftime",
                "format": "flac",
                "store_dir": "/tmp",
            },
        }
        audio_config = recorder.Config.Audio(raw_dict)

        cmd = self.rec.build_record_command(audio_config)
        expected = [
            "nice",
            "-n",
            "18",
            "ionice",
            "-c",
            "2",
            "-n",
            "0",
            "/usr/bin/ffmpeg",
            "-hide_banner",
            "-loglevel",
            "error",
            "-i",
            "default",
            "-f",
            "segment",
            "-segment_atclocktime",
            "1",
            "-segment_time",
            "1800",
            "-strftime",
            "1",
            "/tmp/%Y/%m/%d/strftime.flac",
        ]
        self.assertEqual(cmd, expected)

    def test_create_dirs(self):
        """Test to create the dirs for the next days."""
        with mock.patch("os.makedirs") as mock_oserror:
            mock_oserror.side_effect = PermissionError
            self.assertRaises(PermissionError, self.rec.create_dirs, "store_dir", "%Y")

        with mock.patch("os.makedirs") as mock_oserror:
            mock_oserror.side_effect = FileExistsError
            self.assertTrue(self.rec.create_dirs("store_dir", "%Y"))

        with mock.patch("os.makedirs") as mock_oserror:
            mock_oserror.return_value = None
            self.assertTrue(self.rec.create_dirs("store_dir", "%Y"))

    def test_delete_old_files(self):
        """Test to delete old files."""
        with mock.patch("os.walk") as mock_oserror:
            mock_oserror.return_value = [("root", "dirs", ["file1, file2", ".gitignore"])]

            self.assertFalse(self.rec.delete_old_files("dir_to_check", 1))

    class stat_obj:
        """Dummy stat class."""

        st_mtime = 0

    @patch.multiple(
        "os",
        walk=MagicMock(return_value=[("root", "dirs", ["file1, file2", ".gitignore"])]),
        stat=MagicMock(return_value=stat_obj()),
        remove=MagicMock(return_value=None),
    )
    def test_delete_old_files_mocking_os_stat(self, **mocks):
        """Test to delete old files when mocking os.stat()."""
        self.assertTrue(self.rec.delete_old_files("dir_to_check", 1))

    @patch.multiple(
        "os",
        walk=MagicMock(return_value=[("root", "dirs", ["file1, file2", ".gitignore"])]),
        stat=MagicMock(return_value=stat_obj()),
        remove=MagicMock(side_effect=PermissionError),
    )
    def test_delete_old_files_mocking_permission_error(self, **mocks):
        """Test to delete old files when mocking for a permission error."""
        self.assertFalse(self.rec.delete_old_files("dir_to_check", 1))

    def test_validate_dir(self):
        """Test validate_dir with mocking."""
        with mock.patch("os.access") as mock_oserror:
            mock_oserror.return_value = False
            self.assertFalse(self.rec.validate_dir("store_dir"))
        with mock.patch("os.path.isdir") as mock_oserror:
            mock_oserror.return_value = False
            self.assertFalse(self.rec.validate_dir("store_dir"))

    def test_get_validated_store_dir(self):
        """Test get_validated_store_dir with mocking."""
        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder.validate_dir"
        ) as mock_valdir:
            mock_valdir.return_value = False
            self.assertIsNone(self.rec.get_validated_store_dir(""))
        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder.validate_dir"
        ) as mock_valdir:
            mock_valdir.return_value = True
            self.assertEqual("audio", self.rec.get_validated_store_dir("audio"))
            self.assertEqual("audio", self.rec.get_validated_store_dir("audio/"))

    def test_get_validated_audio_input(self):
        """Test if audio device is selected via cli if none given."""
        self.assertEqual("default", self.rec.get_validated_audio_input("default"))
        with mock.patch("src.engine_recorder.select_pcm.select_pcm") as mock_select_pcm:
            mock_select_pcm.return_value = "default"
            self.assertEqual("default", self.rec.get_validated_audio_input(None))

    def test_is_web_stream(self):
        """Test if web stream are detected."""
        self.assertTrue(self.rec.is_web_stream("http://live.freirad.at"))
        self.assertTrue(self.rec.is_web_stream("https://live.freirad.at"))
        self.assertFalse(self.rec.is_web_stream("hw:1,0"))

    def test_validate_virtual_audio_device(self):
        """Test validating the virtual audio device."""
        raw_dict = {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": "default",
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": 1800,
                "strftime": "strftime",
                "format": "flac",
                "store_dir": "/tmp",
            },
        }
        audio_config = recorder.Config.Audio(raw_dict)
        self.assertFalse(self.rec.validate_virtual_audio_device(audio_config))
        audio_config.input.virtual_device = True
        self.assertTrue(self.rec.validate_virtual_audio_device(audio_config))
        audio_config.input.source = "http://live.freirad.at"
        self.assertFalse(self.rec.validate_virtual_audio_device(audio_config))

    def test_create_virtual_audio_device(self):
        """Test to create a virtual audio device."""
        raw_dict = {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": 2,
                "sample_rate": None,
                "codec": None,
                "source": "default",
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": 1800,
                "strftime": "strftime",
                "format": "flac",
                "store_dir": "/tmp",
            },
        }
        audio_config = recorder.Config.Audio(raw_dict)

        with mock.patch("builtins.open") as mock_oserror:
            mock_oserror.side_effect = PermissionError
            self.assertFalse(self.rec.create_virtual_audio_device(audio_config))

        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder._write_dsnoop_config"
        ) as mock_oserror:
            mock_oserror.return_value = None
            self.assertTrue(self.rec.create_virtual_audio_device(audio_config))

    def test_write_asoundrc_config(self):
        """Test to write asoundrc config with mocking open."""
        with mock.patch("builtins.open", mock.mock_open()) as mock_file:
            self.rec._write_asoundrc_config("test config")
        handle = mock_file()
        handle.write.assert_called_once_with("test config")

    def test_read_asoundrc_config(self):
        """Test to read asoundrc config with mocking open."""
        with mock.patch("builtins.open", mock.mock_open(read_data="test data")):
            self.assertEqual(self.rec._read_asoundrc_config(), "test data")

    def test_write_dsnoop_config(self):
        """Test write dsnoop config with mocking read and write asoundrc config."""
        self.rec.pcm_device_name = "Aura_Recorder"
        sample_config = """pcm.Aura_Recorder {{
    type dsnoop
    ipc_key 111111
}}"""
        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder._read_asoundrc_config"
        ) as mock_read:
            mock_read.return_value = sample_config
            with mock.patch("builtins.open", mock.mock_open()) as mock_file:
                self.rec._write_dsnoop_config("test")

            test_data = """test\n"""
            handle = mock_file()
            handle.write.assert_called_once_with(test_data)

        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder._read_asoundrc_config"
        ) as mock_read:
            mock_read.side_effect = FileNotFoundError
            with mock.patch("builtins.open", mock.mock_open()) as mock_file:
                self.rec._write_dsnoop_config("test")

            test_data = """test"""
            handle = mock_file()
            handle.write.assert_called_once_with(test_data)

    def test_setup_config_error(self):
        """Test setup() for Config errors."""
        with mock.patch(
            "src.engine_recorder.engine_recorder.Recorder.read_yaml_config"
        ) as mock_read_yaml:
            mock_read_yaml.side_effect = recorder.ConfigError
            self.assertRaises(SystemExit, self.rec.setup)

    config_dict = {
        "Audio": {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": None,
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": None,
                "strftime": None,
                "format": None,
                "store_dir": None,
            },
        },
        "Sync": {
            "sync": None,
            "destination": None,
            "options": None,
        },
        "Delete": {"delete": None, "older_than": None},
        "Logger": {"name": "test_aura", "file": None, "level": None},
    }

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value=None),
    )
    def test_setup_wrong_store_dir(self, **mocks):
        """Test the setup with a faulty store_dir."""
        self.assertRaises(SystemExit, self.rec.setup)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(side_effect=PermissionError),
    )
    def test_setup_create_dirs_no_permission(self, **mocks):
        """Test the setup when create dirs fails."""
        self.assertRaises(SystemExit, self.rec.setup)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        validate_virtual_audio_device=MagicMock(return_value=True),
        create_virtual_audio_device=MagicMock(return_value=True),
        create_dirs=MagicMock(side_effect=PermissionError),
    )
    def test_setup_virtual_audio_device(self, **mocks):
        """Test the setup with a virtual audio device."""
        self.assertRaises(SystemExit, self.rec.setup)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        validate_virtual_audio_device=MagicMock(return_value=True),
        create_virtual_audio_device=MagicMock(return_value=False),
    )
    def test_setup_virtual_audio_device_failed(self, **mocks):
        """Test the setup with a virtual audio device but it fails."""
        self.assertRaises(SystemExit, self.rec.setup)

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_setup_no_archiver(self, **mocks):
        """Test the setup with no archiver."""
        record = recorder.Recorder()
        record.setup()
        self.assertEqual(None, record.archiver)

    config_dict_archiver = {
        "Audio": {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": None,
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": None,
                "strftime": None,
                "format": None,
                "store_dir": None,
            },
        },
        "Sync": {
            "sync": True,
            "destination": None,
            "options": None,
        },
        "Delete": {"delete": None, "older_than": None},
        "Logger": {"name": "test_aura", "file": None, "level": None},
    }

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict_archiver),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_setup_archiver(self, **mocks):
        """Test the setup with a archiver."""
        self.rec.setup()
        self.assertTrue(self.rec.config.sync.sync)
        self.assertIsInstance(recorder.Archiver(self.rec.config.logger), recorder.Archiver)

    config_dict_delete = {
        "Audio": {
            "input": {
                "source_format": None,
                "sample_format": None,
                "channels": None,
                "sample_rate": None,
                "codec": None,
                "source": None,
                "virtual_device": None,
            },
            "output": {"codec": None, "bitrate": None},
            "file": {
                "segment_time": None,
                "strftime": None,
                "format": None,
                "store_dir": None,
            },
        },
        "Sync": {
            "sync": None,
            "destination": None,
            "options": None,
        },
        "Delete": {"delete": True, "older_than": None},
        "Logger": {"name": "test_aura", "file": None, "level": None},
    }

    @patch.multiple(
        "src.engine_recorder.engine_recorder.Recorder",
        read_yaml_config=MagicMock(return_value=config_dict_delete),
        get_validated_store_dir=MagicMock(return_value="/tmp"),
        get_validated_audio_input=MagicMock(return_value="default"),
        create_dirs=MagicMock(return_value=True),
    )
    def test_setup_delete(self, **mocks):
        """Test the setup with a deleter."""
        rec = recorder.Recorder()
        rec.setup()
        self.assertTrue(rec.config.delete.delete)


if __name__ == "__main__":
    unittest.main()
