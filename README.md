

# Aura Engine-Recorder

Engine-Recorder is a `tool` that allows `AuRa Users` to record the audio playout of the [Engine](https://gitlab.servus.at/aura/engine).

The Engine-Recorder will run `24/7` to log all the playout from the AuRa Engine, for this it uses `FFmpeg`. It will store the audio files in blocks of configurable length. Furthermore the Recorder is able to store your recordings in a second location using `rsync`. You can also define a time span, files older than this time span can be automatically deleted.

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed the latest version of [Docker](https://www.docker.com/).
* You have a `Linux` machine. The Engine-Recorder has been tested on `Debian` and `Ubuntu`.
* You have a audio device ready to play audio.

---

## Installing Engine-Recorder using Docker Compose

For **production deployments** follow the **Docker Compose installation** instruction for AURA Playout or AURA Recorder at [docs.aura.radio](https://docs.aura.radio/).

---

## Installing Engine-Recorder for development.

The following instructions are meant for development.

### Build with Docker

Build your own, local Docker image

```bash
docker compose build
```

### Configuration

Copy the `sample.env` file to `.env` and edit it. Change your audio Device in `config/asound.conf`. There is also the option to override the `.env` config by copying the [sample file](https://gitlab.servus.at/aura/engine-recorder/-/blob/main/config/sample-engine-recorder.yaml) in `config/sample-engine-recorder.yaml` to `config/engine-recorder.yaml`. For more information about the options check out the [Options](OPTIONS.md) page. This will give you complete control over the settings. Make sure to adapt the paths in the config file in order for them to work in the docker container. For example `./audio` is mapped to `/var/audio` inside the container. 

### Run with Docker

Run the locally build image

```bash
docker compose up
```

---

## Installing Engine-Recorder bare metal

To install Engine-Recorder bare metal check the additional requirements and follow these steps:

### Requirements

- `git`, `make`
- [Python 3.10+](https://www.python.org/downloads/release/python-3100/)
- [Poetry](https://python-poetry.org/)

After that install all dependencies:

```
make init.app
```

For development install with:

```
make init.dev
```

### Configuration

Check out the [sample file](https://gitlab.servus.at/aura/engine-recorder/-/blob/main/config/sample-engine-recorder.yaml) in `config/sample-engine-recorder.yaml`. With `make init.app` this has been copied to `config/engine-recorder.yaml`, change this file to your needs. For more information about the options check out the [Options](OPTIONS.md) page.

### Using Engine-Recorder

To use Engine-Recorder, follow this simple step:

```
make run
```

This starts the Engine-Recorder and after a short startup time it will be recording.

---

## Read more

- [Engine Developer Guide](docs/developer-guide.md)
- [docs.aura.radio](https://docs.aura.radio)
- [aura.radio](https://aura.radio)