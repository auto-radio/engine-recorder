# Options

Here you can find more information about the `config/engine-recorder.yaml` config file and what each option does. 

### Available soundcards and digital audio devices <a name="ava_soundcard"></a>

To list all available soundcards and digital audio devices you can run

```
arecord -l
```

### Available PCMs defined <a name="ava_pcm"></a>

To list all available PCMs defined run

```
arecord -L
```

### Audio device information <a name="ava_info"></a>

To get information about a specific audio device of yours it might be helpful to run

```
arecord -D "hw:0" --dump-hw-params
```

where `"hw:0"` is your audio device. See [available audio devices](#ava_pcm).

---
<br />
<br />

# Audio

## input

### source_format

```
source_format : "alsa"
```

<details>
    <summary>Expand for more</summary>

This option makes use of FFmpegs `-f`. For now AuRa only supports `alsa`. 

>Force input or output file format. The format is normally auto detected for input sources and guessed from the file extension for output files, so this option is not needed in most cases.

#### Examples

```
source_format : "alsa"
```

</details>


### Optional: sample_format

```
sample_format : "pcm_s32le"
```

<details>
    <summary>Expand for more</summary>

This option makes use of FFmpegs `-acodec`. 

>Set the audio codec. This is an alias for "-codec:a".

A list of supported sample formats can be shown with

```
ffmpeg -formats | grep PCM
```

To check which sample formats your device supports see [Audio device information](#ava_info).

#### Examples

```
sample_format : "pcm_s32le"
```

</details>

### channels

```
channels : 2
```

<details>
    <summary>Expand for more</summary>

This option makes use off FFmpegs `-ac`.

>Set the number of audio channels. For output streams it is set by default to the number of input audio channels. For input streams this option only makes sense for audio grabbing devices and raw demuxers and is mapped to the corresponding demuxer options.

#### Examples

```
channels : 2
```

```
channels : 1
```

</details>

### Optional: sample_rate

```
sample_rate : 44100
```

<details>
    <summary>Expand for more</summary>

This option makes use off FFmpegs `-ar`.

>Set the audio sampling frequency. For output streams it is set by default to the frequency of the corresponding input stream. For input streams this option only makes sense for audio grabbing devices and raw demuxers and is mapped to the corresponding demuxer options. 

To check which sampling frequency your device supports see [Audio device information](#ava_info).

#### Examples

```
sample_rate : 44100
```

```
sample_rate : 48000
```

</details>

### Optional: codec

```
codec : "flac"
```

<details>
    <summary>Expand for more</summary>

This option makes use off FFmpegs `-codec`.

>Select an encoder (when used before an output file) or a decoder (when used before an input source) for one or more streams. codec is the name of a decoder/encoder or a special value copy (output only) to indicate that the stream is not to be re-encoded.

#### Examples

```
codec : "flac"
```

```
codec : "libvorbis"
```

```
codec_input :
```

</details>

### source

```
file : "default"
```

<details>
    <summary>Expand for more</summary>

This option describes the input source to listen to, it can be a pcm device or a stream. If left empty the Engine-Recorder will let you choose an option from [available devices](#ava_pcm) cli interface. It makes use of FFmpegs `-i`. 

>input source url 

#### Examples

```
file :
```

```
file : "default"
```

```
file : "dsnoop:CARD=USB,DEV=0"
```

```
file : "http://stream.freirad.at:8002/live.ogg"
```

</details>

### Optional: virtual_device

```
virtual_device : False
```

<details>
    <summary>Expand for more</summary>

This option allows you to create a simple dsnoop audio device. It will try to create a dsnoop configuration of the [input_source](#input_source).

`Alsa` does this out of the box. There should be a dsnoop device for every pcm device. Check [Available PCMs](ava_pcm) for more information. 

**Always** try to use the dsnoop device provided by `Alsa` first for better performance.

#### Examples

```
virtual_device : False
```

```
virtual_device : True
```

</details>

## output

### codec <a name="codec_output"></a>

```
codec : "flac"
```

<details>
    <summary>Expand for more</summary>

This option makes use off FFmpegs `-codec`.

>Select an encoder (when used before an output file) or a decoder (when used before an input source) for one or more streams. codec is the name of a decoder/encoder or a special value copy (output only) to indicate that the stream is not to be re-encoded.

## Examples

```
codec : "flac"
```

```
codec : "libvorbis"
```

</details>

### Optional: bitrate

```
bitrate : "92k"
```

<details>
    <summary>Expand for more</summary>

This option makes use off FFmpegs `-ab`.

>Some options are applied per-stream, e.g. bitrate or codec. Stream specifiers are used to precisely specify which stream(s) a given option belongs to.
>
>A stream specifier is a string generally appended to the option name and separated from it by a colon. E.g. -codec:a:1 ac3 contains the a:1 stream specifier, which matches the second audio stream. Therefore, it would select the ac3 codec for the second audio stream.
>
>A stream specifier can match several streams, so that the option is applied to all of them. E.g. the stream specifier in -b:a 128k matches all audio streams. 

#### Examples

```
bitrate :
```

```
bitrate : "92k"
```

</details>

## file

### segment_time

```
segment_time : 1800
```

<details>
    <summary>Expand for more</summary>

This option describes the time length of one recording block in seconds. It makes use off FFmpegs `-segment_time`.

>Set segment duration to time, the value must be a duration specification. Default value is "2". See also the segment_times option.
>
>Note that splitting may not be accurate, unless you force the reference stream key-frames at the given time. See the introductory notice and the examples below.

#### Examples

```
segment_time : 1800
```

</details>

### strftime <a name="strftime"></a>

```
strftime : "%Y-%m-%d_%H:%M:%S_%s"
```

<details>
    <summary>Expand for more</summary>

This option describes the way a segment is named. It is important to note that the Engine-Recorder always creates the directory tree `$store_dir/YYYY/MM/DD` to save the segments in.

This option makes use of the underlying FFmpeg option `-strftime` which is set to `1`.

>Use the strftime function to define the name of the new segments to write. If this is selected, the output segment name must contain a strftime function template.

#### Examples

```
strftime : "%Y-%m-%d_%H:%M:%S_%s"
```

```
strftime : "%Y%m%d_%H%M%S"
```

</details>


### format

```
format : "flac"
```

<details>
    <summary>Expand for more</summary>

This option describes the file format in which the audio segments are saved. It should be the matching file format for the [output codec](#codec_output).

It is automatically append to the [strftime string](#strftime).

#### Examples

```
format : "flac"
```

```
format : "ogg"
```

```
format : "mp3"
```

</details>

### store_dir

```
store_dir : 
```

<details>
    <summary>Expand for more</summary>

This option describes the directory in which all audio segments will be saved. If left blank `audio/recordings/block` is used.

#### Examples

```
store_dir : 
```

```
store_dir : "/tmp/audio"
```

</details>

---

# Sync

## Optional: sync

```
sync : False
```

<details>
    <summary>Expand for more</summary>

This options allows you to synchronize to content of `store_dir` to another directory or location. Once a day the Engine-Recorder will check if there any files to synchronize. This functionality uses rsync for that.

### Examples

```
sync : False
```

```
sync : True
```

</details>

## destination

```
destination : "/tmp/audio/"
```

<details>
    <summary>Expand for more</summary>

This option sets the destination for `rsync`. To use this `sync` must be set to `True`. The destination can be a remote location. If you chose a remote location, make sure to supply a ssh-key, otherwise Engine-Recorder will ask you for a password every time it tries to synchronize.

### Examples

```
destination : "/tmp/audio/"
```

```
destination : "user@remote_server:/tmp/audio/"
```

</details>

## options

```
options : "-a"
```

<details>
    <summary>Expand for more</summary>

This options tells `sync` which options to apply. It requires `sync` to be set to `True`. The supplied options are not allowed to have space in between them.

### Examples

```
options : "-a"
```

```
options : "-apogt"
```

</details>

---

# Delete

## delete

```
delete : False
```

<details>
    <summary>Expand for more</summary>

This options allows you to delete audio files in `store_dir` older then `x` days.

### Examples

```
delete : False
```

```
delete : True
```

</details>

## older_than

```
older_than : 7 # Days
```

<details>
    <summary>Expand for more</summary>

This option describes the time span which is used to check if a file should be deleted or not. Enter in days. This requires `delete` to be set to `True`.

### Examples

```
older_than : 7 # Days
```

```
older_than : 1
```

</details>

# Logger


## name

```
name : "Engine-Recorder"
```

<details>
    <summary>Expand for more</summary>

This option sets the name for the logger. This name shows up in the log file.

### Examples

```
name : "Engine-Recorder"
```

```
name : "Recorder"
```

</details>

## file

```
file : 
```

<details>
    <summary>Expand for more</summary>

This option allows to set a custom log file.

### Examples

```
file : "/tmp/logfile.log"
```

```
file :
```

</details>

## level

```
level : "INFO"
```

<details>
    <summary>Expand for more</summary>

This option describes how verbose the Engine-Recorder is.
Available options are:

- `"DEBUG"`
- `"INFO"`
- `"WARNING"`
- `"ERROR"`

### Examples

```
level : "INFO"
```

```
level : "ERROR"
```

</details>