-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    lint            - verify code style"
	@echo "    spell           - check spelling of text"
	@echo "    format          - apply automatic formatting"
	@echo "    test            - run the test suite"
	@echo "    coverage        - check the coverage of the test suite"
	@echo "    log             - tail log file"
	@echo "    run             - run the engine-recorder"
	@echo "    release         - tag and push release with current version"
	$(call docker_help)

TIMEZONE := "Europe/Vienna"
AUDIO_DEVICE := ${AUDIO_DEVICE}
AUDIO_STORE := ${AUDIO_DEVICE}

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--mount type=tmpfs,destination=/tmp \
		--device /dev/snd \
		--group-add audio \
		-e TZ=$(TIMEZONE) \
		-e AUDIO_DEVICE=$(AUDIO_DEVICE) \
		-v "$(AUDIO_STORE)":"/var/audio" \
		-u $(UID):$(GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)


copy::
	@test -f "config/engine-recorder.yaml" || cp config/sample-engine-recorder.yaml config/engine-recorder.yaml

init.app : pyproject.toml poetry.lock
	poetry install --no-dev
	cp config/sample-engine-recorder.yaml config/engine-recorder.yaml

init.dev : pyproject.toml poetry.lock
	poetry install
	cp config/sample-engine-recorder.yaml config/engine-recorder.yaml

lint::
	poetry run python3 -m flake8 .

spell::
	poetry run codespell $(wildcard *.md) docs src tests config contrib

format::
	poetry run isort .
	poetry run black .

coverage::
	poetry run coverage run --source src.engine_recorder -m unittest discover -b
	poetry run coverage xml

test::
	poetry run python3 -m xmlrunner discover tests --output-file report.xml

log::
	tail -f log/engine-recorder.log

run::
	poetry run start

release:: VERSION := $(shell python3 -c 'import tomli; print(tomli.load(open("pyproject.toml", "rb"))["tool"]["poetry"]["version"])')
release::
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."