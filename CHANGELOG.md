# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

### Changed

- ...

### Deprecated

- ...

### Removed

- ...

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha3] - TBA

### Added

- Added a option to configure the sub directories where the audio files are saved (#36)
- Added local docker build (#31)
- Added docker compose

### Changed

- Renamed the input.file field to input.source (#32)
- Renamed the input.file_format field to input.source_format (#32)

### Fixed

- Fixed a bug where the audio files where always saved with the `_%s` suffix (#35)

## [1.0.0-alpha2] - 2023-06-23

### Fixed

- Fixed a bug where the recorder would not start when trying to record a web stream (aura#182)

## [1.0.0-alpha1] - 2023-02-24

Initial release.
