# Docker targets for AURA Makefiles


# Help

define docker_help
	@echo "    docker.build     - build docker image"
	@echo "    docker.build.dev - build docker image with user"
	@echo "    docker.push      - push docker image"
	@echo "    docker.run       - start app in container"
	@echo "    docker.run.i     - start app in container (interactive mode)"
	@echo "    docker.run.bash  - start bash in container"
	@echo "    docker.restart   - restart container"
	@echo "    docker.stop      - stop container"
	@echo "    docker.rm        - stop and remove container"
	@echo "    docker.log       - container logs for app"
	@echo "    docker.bash      - enter bash in running container"
endef

ifeq ($(user),)
# USER retrieved from env, UID from shell.
HOST_USER ?= $(strip $(if $(USER),$(USER),nodummy))
HOST_UID ?= $(strip $(if $(shell id -u),$(shell id -u),666666))
HOST_GID ?= $(strip $(if $(shell id -g),$(shell id -g),666666))
else
# allow override by adding user= and/ or uid=  (lowercase!).
# uid= defaults to 0 if user= set (i.e. root).
HOST_USER = $(user)
HOST_UID = $(strip $(if $(uid),$(uid),0))
HOST_GID = $(strip $(if $(gid),$(gid),0))
endif

@echo "HOST_UID: $(HOST_UID)"

docker.deps:
	@which docker

# Targets

docker.build:: docker.deps
	@docker build -t autoradio/$(APP_NAME) .

docker.build.dev:: docker.deps
	@echo "HOST_UID: $(HOST_UID)"
	@echo "HOST_GID: $(HOST_GID)"
	@docker build --build-arg HOST_UID=$(HOST_UID) --build-arg HOST_GID=$(HOST_GID) --build-arg HOST_USER=$(HOST_USER) -t autoradio/$(APP_NAME) .

docker.push:: docker.deps
	@docker push autoradio/$(APP_NAME)

docker.run:: DOCKER_ENTRY_POINT := -d
docker.run:: docker.deps
	$(DOCKER_RUN)

docker.run.i:: DOCKER_ENTRY_POINT := -it
docker.run.i:: docker.deps
	$(DOCKER_RUN)

docker.run.bash:: DOCKER_ENTRY_POINT := -v "$(CURDIR)":"/srv" --entrypoint bash -it
docker.run.bash:: docker.deps
	$(DOCKER_RUN)

docker.restart:: docker.deps
	@docker restart $(APP_NAME)

docker.stop:: docker.deps
	@docker stop $(APP_NAME)

docker.rm:: docker.stop
	@docker rm $(APP_NAME)

docker.log:: docker.deps
	@docker logs $(APP_NAME) -f

docker.bash:: docker.deps
	@docker exec -it $(APP_NAME) bash
