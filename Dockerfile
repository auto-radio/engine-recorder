FROM python:3.9-slim-bookworm
LABEL maintainer="Loxbie <ole@freirad.at>"

# System Dependencies
RUN apt-get update && apt-get -y install \
      apt-utils \
      alsa-utils \
      build-essential \
      ffmpeg \
      rsync \
      libffi-dev \
      libssl-dev \
      pkg-config \
      curl

RUN   \
      if [ `dpkg --print-architecture` = "armhf" ]; then \
      printf "[global]\nextra-index-url=https://www.piwheels.org/simple\n" > /etc/pip.conf ; \
      fi

RUN pip install cryptography poetry

# Setup Engine-Recorder
ENV TZ=Europe/Vienna
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /srv
RUN mkdir -p /srv/src
RUN mkdir -p /srv/config
RUN mkdir -p /srv/log
RUN mkdir -p /var/audio/recordings/block

COPY poetry.lock /srv
COPY pyproject.toml /srv
COPY src /srv/src
COPY config/for-docker.yaml /srv/config/engine-recorder.yaml
COPY README.md /srv
COPY Makefile /srv


# Define Aura User
ARG AURA_UID=2872
ARG AURA_GID=${AURA_UID}

RUN groupadd --gid ${AURA_GID} aura && \
      useradd --gid ${AURA_GID} --no-user-group --uid ${AURA_UID} --home-dir /srv --no-create-home aura && \
      chown -R ${AURA_UID}:${AURA_GID} /srv /var/audio


USER aura

WORKDIR /srv
RUN poetry install --without dev --no-interaction

# Start the Engine
ENTRYPOINT ["make"]
CMD ["run"]
